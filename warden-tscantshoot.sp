#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Warden - Ts Can't Shoot"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "zipcore.net"

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <smlib>
#include <multicolors>
#include <warden>

bool g_bActive;
int g_iWepType[MAXPLAYERS + 1];

public void OnPluginStart()
{
	CreateConVar("warden_tscantshoot_version", PLUGIN_VERSION, "Warden - Ts Can't Shoot Version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	HookEvent("round_end", Event_RoundEnd);
	HookEvent("item_equip", Event_ItemEquip);
	
	RegConsoleCmd("sm_disable_t_attack", Cmd_NoShoot);
}

public void OnMapStart()
{
	g_bActive = false;
}

public Action Event_RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	g_bActive = false;
	
	return Plugin_Continue;
}

public Action Event_ItemEquip(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_iWepType[client] = GetEventInt(event, "weptype");
}

public Action Cmd_NoShoot(int client, int args)
{
	if(client > 0 && !warden_iswarden(client))
	{
		CPrintToChat(client, "{darkred}Only the warden can use this command!");
		return Plugin_Handled;
	}
	
	CPrintToChat(client, "{darkred}Ts can't shoot until end of round. Only knives can be used by them!");
	
	g_bActive = true;
	
	return Plugin_Handled;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(g_bActive && g_iWepType[client] != 0 && IsPlayerAlive(client) && GetClientTeam(client) == CS_TEAM_T)
		BlockWeapon(client, weapon, 0.5);
  	
  	return Plugin_Continue;
}

void BlockWeapon(int client, int weapon, float time)
{
	float fUnlockTime = GetGameTime() + time;
	
	SetEntPropFloat(client, Prop_Send, "m_flNextAttack", fUnlockTime);
	
	if(weapon > 0)
		SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", fUnlockTime);
}