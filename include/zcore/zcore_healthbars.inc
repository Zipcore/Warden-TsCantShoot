#if defined _zcore_healthbars_included
 #endinput
#endif
#define _zcore_healthbars_included

native int ZCore_Healthbar_AddPack(char[] sprite_pack);
native int ZCore_Healthbar_Set(int client, float zpos, int maxhp, char[] sprite_pack, float scale, float lifetime, int immunity);
native int ZCore_Healthbar_Remove(int client, int immunity);

public void __pl_zcore_healthbars_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_Healthbar_AddPack");
	MarkNativeAsOptional("ZCore_Healthbar_Set");
	MarkNativeAsOptional("ZCore_Healthbar_Reset");
}