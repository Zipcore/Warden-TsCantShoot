#if defined _zcore_pipe_included
 #endinput
#endif
#define _zcore_pipe_included

forward void ZCore_Pipe_OnDataReceive(char[] msg, bool master);
forward void ZCore_Pipe_OnDataSend(char[] msg, bool master);

native int ZCore_Pipe_SendData(char[] msg);
native bool ZCore_Pipe_IsMasterServer();

public void __pl_zcore_pipe_SetNTVOptional() 
{
	MarkNativeAsOptional("ZCore_Pipe_SendData");
	MarkNativeAsOptional("ZCore_Pipe_IsMasterServer");
}

stock int GetClientBySteamID(char[] steamid)
{
	char authid[32];
	char legacy[32];
	for(new i=1;i<=MaxClients;++i)
	{
		if(!IsClientInGame(i))
			continue;
		if(!IsClientAuthorized(i))
			continue;
			
		GetClientAuthId(i, AuthId_Steam2, authid, 32);
		GetClientAuthId(i, AuthId_Steam2, legacy, 32);
		if(strcmp(legacy[8], steamid[8])==0 || strcmp(authid, steamid)==0)
			return i;
	}
	return 0;
}