#if defined _rp_buildings_included
 #endinput
#endif
#define _rp_buildings_included

enum RP_Buildings
{
	Building_ID,
	String:Building_Name[64],
	Building_OwnerID,
	String:Building_OwnerName[64],
	Building_Type
}

enum
{
	BD_Default,
	BD_PoliceDepartment,
	BD_Bank,
	BD_HomeHouse,
	BD_Gangbase,
	BD_AppartmentHouse	
}

native RP_GetMaxBuildingCount();
native RP_GetBuildingIDByArrayIndex(index);

native RP_GetBuildingName(buildingID,String:buffer[], length_64);
native RP_GetBuildingOwnerID(buildingID);
native RP_GetBuildingOwnerName(buildingID,String:buffer[], length_64);
native RP_GetBuildingType(buildingID);

native RP_SetBuildingName(buildingID,String:buffer[]);
native RP_SetBuildingOwnerID(buildingID,new_ownerID);
native RP_SetBuildingOwnerName(buildingID,String:buffer[]);
native RP_SetBuildingType(buildingID,new_type);

native RP_CreateBuilding(String:buffer[]);


public __pl_rp_buildings_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetMaxBuildingCount");
	MarkNativeAsOptional("RP_GetBuildingIDByArrayIndex");
	
	MarkNativeAsOptional("RP_GetBuildingName");
	MarkNativeAsOptional("RP_GetBuildingOwnerID");
	MarkNativeAsOptional("RP_GetBuildingOwnerName");
	MarkNativeAsOptional("RP_GetBuildingType");
	
	MarkNativeAsOptional("RP_SetBuildingName");
	MarkNativeAsOptional("RP_SetBuildingOwnerID");
	MarkNativeAsOptional("RP_SetBuildingOwnerName");
	MarkNativeAsOptional("RP_SetBuildingType");
	
	MarkNativeAsOptional("RP_CreateBuilding");
}