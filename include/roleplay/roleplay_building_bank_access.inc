#if defined _rp_building_bank_doors_included
 #endinput
#endif
#define _rp_building_bank_doors_included

enum RP_BankAccess
{
	BankAccess_PlayerID,
	BankAccess_AccessID,
	BankAccess_BuildingID
}

enum
{
	Bank_Doors_Manager_Access,
	Bank_Doors_Worker_Access,
	Bank_Doors_Costumer_Access
}

native RP_GetPlayerBankAccess(client, buildingID);
native RP_SetPlayerBankAccess(client, buildingID, new_accessID);


public __pl_rp_building_bank_doors_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetPlayerBankAccess");
	MarkNativeAsOptional("RP_SetPlayerBankAccess");
}