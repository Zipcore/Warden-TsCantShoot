#if defined _rp_homes_manager_included
 #endinput
#endif
#define _rp_homes_manager_included


native RP_GetHomeDoorCount();
native RP_GetPlayerAccess(playerID, homeID);

public __pl_rp_homes_manager_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetHomeDoorCount");
	MarkNativeAsOptional("RP_GetPlayerAccess");
}