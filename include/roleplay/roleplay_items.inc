#if defined _rp_items_included
 #endinput
#endif
#define _rp_items_included

#define MAX_ITEM_SETTINGS 5

#define MAX_FEATURE_LENGTH 256

/* Item Types */ 
/* Notice: Only pre difined item types, optional plugins can use other typeIDs but you have to about double usage of the same typeID */
enum
{
	IT_Other, //0
	IT_Valuable, //1
	IT_Furniture, //2
	IT_SpecialWeapon, //3
	IT_Weapon, //4
	IT_Ammo, //5
	IT_Food, //6
	IT_Drink, //7
	IT_Medic, //8
	IT_Tool, //9
	IT_Drug, //10
	IT_Document, //11
	IT_Badge, //12
	IT_Tattoo, //13
	IT_Skin, //14
	IT_Mask, //15
	IT_Home //16
}

enum RP_ItemType
{
	ItemType_ID,
	ItemType_Name
}

enum RP_Item
{
	Item_ID,
	Item_Type, //Item type
	Item_SubType, //Item suptype
	Item_WorthBuy, //How much you need to buy this
	Item_WorthSell, //How much you get for selling this
	Item_Durability, //How many times this item is stacked
	Item_Weight, //Weight...
	Item_Team, //Make an item only available for Ts or CTs
	Item_ReqLevel, //Min level required for this item
	Item_ReqItem, //This item requires another item to own it
	Item_ReqItemDurability, //How many times you need that item
	Item_CrimePickup, //Crime a player get for pickup this item
	Item_CrimeBuy, //Crime a player get for biying this item
	Item_CrimeSell, //Crime a player get for sellingdroppingselling this item
	Item_CrimeUse, //Crime a player gets by using this item
	bool:Item_Tradable, //Allow item to be traded with other players by a player
	bool:Item_Dropable, //Allow item to be dropped by a player
	bool:Item_Useable, //Allow item to be used by a player
	bool:Item_Deleteable, //Allow item to be deleted by a player
	bool:Item_VipOnly, //Is this item only for vips?
	String:Item_Name[64], // Name
	String:Item_Classname[64], // classname, when it's spawnable
	String:Item_Model[128], // model
	String:Item_Features[MAX_FEATURE_LENGTH], //features
	String:Item_Settings[MAX_FEATURE_LENGTH], //settings
	String:Item_Description[512] //Item description
}

enum RP_Inventory
{
	Inventory_ID,
	Inventory_Durability,
	Inventory_DateCreated
}

/* Natives: Items */

/* How many different items exist */
native RP_GetItemCount();

/* Get itemID by cache index */
native RP_GetItemID(index);

/* Get Used Item Type */
native RP_GetItemTypeUsed(index);
native RP_GetItemTypeName(index, String:buffer[], length_64);
native RP_GetMaxItemType();

/* Type of an item */
native RP_GetItemType(itemID);

/* Subtype of an item */
native RP_GetItemSubType(itemID);

/* Buy price*/
native RP_GetItemWorthBuy(itemID);
/* Sell worth*/
native RP_GetItemWorthSell(itemID);

/* How often this item can be used */
native RP_GetItemDurability(itemID);

/* Level which is required to pickup/use/use/sell/buy the item */
native RP_GetItemReqLevel(itemID);

/* Item which is required to pickup/use/use/sell/buy the item */
native RP_GetItemReqItem(itemID);

/* How much durability the required item needs at least */
native RP_GetItemReqItemDurability(itemID);

/* Weight of this item */
native RP_GetItemWeight(itemID);

/* Item is only pickup-/use-/use-/sell-/buyable for one team */
native RP_GetItemTeam(itemID);

/* Crime for adding item to inventory */
native RP_GetItemCrimePickup(itemID);

/* Crime for buying this item */
native RP_GetItemCrimeBuy(itemID);

/* Crime for selling this item */
native RP_GetItemCrimeSell(itemID);

/* Crime for using this item */
native RP_GetItemCrimeUse(itemID);

/* Event Items as GetTime() format */
native RP_GetItemExpirationDate(itemID);

native RP_IsItemTradable(itemID);
native RP_IsItemDropable(itemID);
native RP_IsItemUseable(itemID);
native RP_IsItemDeleteable(itemID);
native RP_IsItemVipOnly(itemID);

native RP_GetItemName(itemID, String:buffer[], length_64);
native RP_GetItemClassname(itemID, String:buffer[], length_64);
native RP_GetItemModel(itemID, String:buffer[], length_128);
native RP_GetItemFeatures(itemID, String:buffer[], length_256);
native RP_GetItemSettings(itemID, String:buffer[], length_256);
native RP_GetItemDescription(itemID, String:buffer[], length_512);

/* Natives: Player Inventory */
native RP_GetPlayerInventoryItemCount(client);
native RP_GetPlayerInventoryItemTypeCount(client, type);
native RP_GetPlayerInventoryItemSubTypeCount(client, type, subType);
native RP_GetPlayerInventoryItem(client, index);

native RP_AddPlayerInventoryItem(client, itemID, durability);
native RP_UsedPlayerInventoryItem(client, itemID, durability);
native RP_RemovePlayerInventoryItem(client, itemID);
native RP_RemovePlayerInventoryItemType(client, RP_ItemType:type);
native RP_RemovePlayerInventoryItemCrime(client);
native RP_HasPlayerInventoryItem(client, itemID);

/* Global cache for entitys */
native RP_GetEntityItem(entity);
native RP_SetEntityItem(entity, itemID);

public __pl_rp_items_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetItemCount");
	MarkNativeAsOptional("RP_GetItemID");
	
	MarkNativeAsOptional("RP_GetItemTypeUsed");
	MarkNativeAsOptional("RP_GetItemTypeName");
	MarkNativeAsOptional("RP_GetMaxItemType");
	
	MarkNativeAsOptional("RP_GetItemType");
	MarkNativeAsOptional("RP_GetItemSubType");
	MarkNativeAsOptional("RP_GetItemWorthBuy");
	MarkNativeAsOptional("RP_GetItemWorthSell");
	MarkNativeAsOptional("RP_GetItemDurability");
	MarkNativeAsOptional("RP_GetItemReqLevel");
	MarkNativeAsOptional("RP_GetItemReqItem");
	MarkNativeAsOptional("RP_GetItemReqItemDurability");
	
	MarkNativeAsOptional("RP_IsItemTradable");
	MarkNativeAsOptional("RP_IsItemDropable");
	MarkNativeAsOptional("RP_IsItemUseable");
	MarkNativeAsOptional("RP_IsItemDeleteable");
	MarkNativeAsOptional("RP_IsItemVipOnly");
	
	MarkNativeAsOptional("RP_GetItemWeight");
	
	MarkNativeAsOptional("RP_GetItemTeam");
	
	MarkNativeAsOptional("RP_GetItemCrimePickup");
	MarkNativeAsOptional("RP_GetItemCrimeBuy");
	MarkNativeAsOptional("RP_GetItemCrimeSell");
	MarkNativeAsOptional("RP_GetItemCrimeUse");
	
	MarkNativeAsOptional("RP_GetItemName");
	MarkNativeAsOptional("RP_GetItemClassname");
	MarkNativeAsOptional("RP_GetItemModel");
	MarkNativeAsOptional("RP_GetItemFeatures");
	MarkNativeAsOptional("RP_GetItemSettings");
	MarkNativeAsOptional("RP_GetItemDescription");
	
	MarkNativeAsOptional("RP_GetPlayerInventoryItemCount");
	MarkNativeAsOptional("RP_GetPlayerInventoryItem");
	
	MarkNativeAsOptional("RP_AddPlayerInventoryItem");
	MarkNativeAsOptional("RP_UsedPlayerInventoryItem");
	MarkNativeAsOptional("RP_RemovePlayerInventoryItem");
	MarkNativeAsOptional("RP_RemovePlayerInventoryItemType");
	MarkNativeAsOptional("RP_RemovePlayerInventoryItemCrime");
	MarkNativeAsOptional("RP_HasPlayerInventoryItem");
	
	MarkNativeAsOptional("RP_GetEntityItem");
	MarkNativeAsOptional("RP_SetEntityItem");
}