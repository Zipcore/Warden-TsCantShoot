#if defined _rp_building_appartmenthouse_access_included
 #endinput
#endif
#define _rp_building_appartmenthouse_access_included

enum RP_AppartmentAccess
{
	AppartmentAccess_PlayerID,
	AppartmentAccess_AccessID,
	AppartmentAccess_AppartementID
}

enum
{
	AppartmentHouse_Manager_Access,
	AppartmentHouse_Worker_Access,
	AppartmentHouse_Mainhabitant_Access,
	AppartmentHouse_Payer_Access,
	AppartmentHouse_Cohabitant_Access
}

native RP_GetPlayerAppartmentAccess(client, buildingID);
native RP_SetPlayerAppartmentAccess(client, buildingID, new_accessID);

public __pl_rp_building_appartmenthouse_access_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_GetPlayerAppartmentAccess");
	MarkNativeAsOptional("RP_SetPlayerAppartmentAccess");
}