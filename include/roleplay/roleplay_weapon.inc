#if defined _rp_weapon_included_
  #endinput
#endif
#define _rp_weapon_included_

stock bool:IsWeaponKnife(const String:weapon[])
{
	if(StrEqual(weapon, "weapon_knife") || StrEqual(weapon, "weapon_weapon_taser"))
	{return true;}
	else
	{return false;}
		
}

stock bool:IsWeaponPrimary(const String:weapon[])
{
	if(StrEqual(weapon, "weapon_ak47") 	|| StrEqual(weapon, "weapon_awp") 		|| StrEqual(weapon, "weapon_aug") 		|| StrEqual(weapon, "weapon_bizon") ||
	StrEqual(weapon, "weapon_famas") 	|| StrEqual(weapon, "weapon_g3sg1") 	|| StrEqual(weapon, "weapon_galilar") 	|| StrEqual(weapon, "weapon_m249") 	||
	StrEqual(weapon, "weapon_m4a1") 	|| StrEqual(weapon, "weapon_mac10") 	|| StrEqual(weapon, "weapon_mag7") 		|| StrEqual(weapon, "weapon_mp7") 	||
	StrEqual(weapon, "weapon_mp9") 		|| StrEqual(weapon, "weapon_negev") 	|| StrEqual(weapon, "weapon_nova") 		|| StrEqual(weapon, "weapon_p90") 	||
	StrEqual(weapon, "weapon_sawedoff") || StrEqual(weapon, "weapon_scar20") 	|| StrEqual(weapon, "weapon_sg556")		|| StrEqual(weapon, "weapon_ssg08") ||
	StrEqual(weapon, "weapon_ump45") 	|| StrEqual(weapon, "weapon_xm1014") 	|| StrEqual(weapon, "weapon_m4a1_silencer"))
	{return true;}
	else
	{return false;}
		
}

stock bool:IsWeaponSecoundary(const String:weapon[])
{
	if(StrEqual(weapon, "weapon_deagle") 	|| StrEqual(weapon, "weapon_elite") 	|| StrEqual(weapon, "weapon_fiveseven")	||
	StrEqual(weapon, "weapon_glock") 		|| StrEqual(weapon, "weapon_hkp2000") 	|| StrEqual(weapon, "weapon_p250") 		|| StrEqual(weapon, "weapon_tec9") 	||
	StrEqual(weapon, "weapon_usp_silencer"))
	{return true;}
	else
	{return false;}
		
}

stock bool:IsWeaponGrenade(const String:weapon[])
{
	if(StrEqual(weapon, "weapon_decoy") || StrEqual(weapon, "weapon_flashbang") || StrEqual(weapon, "weapon_hegrenade") || StrEqual(weapon, "weapon_incgrenade") ||
	StrEqual(weapon, "weapon_molotov") 	|| StrEqual(weapon, "weapon_smokegrenade"))
	{return true;}
	else
	{return false;}
		
}

stock WeaponGetSlot(const String:weapon[])
{
	if(StrEqual(weapon, "weapon_knife") || StrEqual(weapon, "weapon_weapon_taser"))
	{return CS_SLOT_KNIFE;}
	
	else if(StrEqual(weapon, "weapon_deagle") 	|| StrEqual(weapon, "weapon_elite") 	|| StrEqual(weapon, "weapon_fiveseven") ||
	StrEqual(weapon, "weapon_glock") 		|| StrEqual(weapon, "weapon_hkp2000") 	|| StrEqual(weapon, "weapon_p250") 		|| StrEqual(weapon, "weapon_tec9") 	||
	StrEqual(weapon, "weapon_usp_silencer"))
	{return CS_SLOT_SECONDARY;}
	
	else if(StrEqual(weapon, "weapon_ak47") 	|| StrEqual(weapon, "weapon_awp") 		|| StrEqual(weapon, "weapon_aug") 		|| StrEqual(weapon, "weapon_bizon") ||
	StrEqual(weapon, "weapon_famas") 	|| StrEqual(weapon, "weapon_g3sg1") 	|| StrEqual(weapon, "weapon_galilar") 	|| StrEqual(weapon, "weapon_m249") 	||
	StrEqual(weapon, "weapon_m4a1") 	|| StrEqual(weapon, "weapon_mac10") 	|| StrEqual(weapon, "weapon_mag7") 		|| StrEqual(weapon, "weapon_mp7") 	||
	StrEqual(weapon, "weapon_mp9") 		|| StrEqual(weapon, "weapon_negev") 	|| StrEqual(weapon, "weapon_nova") 		|| StrEqual(weapon, "weapon_p90") 	||
	StrEqual(weapon, "weapon_sawedoff") || StrEqual(weapon, "weapon_scar20") 	|| StrEqual(weapon, "weapon_sg556")		|| StrEqual(weapon, "weapon_ssg08") ||
	StrEqual(weapon, "weapon_ump45") 	|| StrEqual(weapon, "weapon_xm1014") 	|| StrEqual(weapon, "weapon_m4a1_silencer"))
	{return CS_SLOT_PRIMARY;}
	
	else if(StrEqual(weapon, "weapon_decoy") || StrEqual(weapon, "weapon_flashbang") || StrEqual(weapon, "weapon_hegrenade") || StrEqual(weapon, "weapon_incgrenade") ||
	StrEqual(weapon, "weapon_molotov") 	|| StrEqual(weapon, "weapon_smokegrenade"))
	{return CS_SLOT_GRENADE;}
	
	else
	{return CS_SLOT_C4;}
}