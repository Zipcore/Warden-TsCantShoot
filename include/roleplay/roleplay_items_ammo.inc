#if defined _rp_items_ammo_included
 #endinput
#endif
#define _rp_items_ammo_included

native RP_GetPlayerPrimaryAmmo(client);
native RP_GetPlayerSecondaryAmmo(client);
native RP_GetPlayerPrimaryAmmoType(client);
native RP_GetPlayerSecondaryAmmoType(client);

public __pl_rp_items_ammo_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_UseWeaponItem");
	MarkNativeAsOptional("RP_GetPlayerPrimaryAmmo");
	MarkNativeAsOptional("RP_GetPlayerSecondaryAmmo");
	MarkNativeAsOptional("RP_GetPlayerPrimaryAmmoType");
	MarkNativeAsOptional("RP_GetPlayerSecondaryAmmoType");
}