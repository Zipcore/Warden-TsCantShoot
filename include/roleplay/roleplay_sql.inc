#if defined _rp_sql_included
 #endinput
#endif
#define _rp_sql_included

forward RP_OnSqlConnected(Handle:sql);
forward RP_OnSqlStop();

native RP_GetSql();

public __pl_rp_sql_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_SqlGetConnection");
}