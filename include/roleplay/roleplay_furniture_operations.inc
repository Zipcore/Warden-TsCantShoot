#if defined _rp_items_furniture_operations_included
 #endinput
#endif
#define _rp_items_furniture_operations_included

stock Entity_RotateProp(entity, Float:x_cord,Float:y_cord,Float:z_cord, bool:_bReset)
{
	new Float:_fValue[3];
	new Float:_fAngles[3];

	_fValue[0] = x_cord;
	_fValue[1] = y_cord;
	_fValue[2] = z_cord;

	if(!_bReset)
	{
		GetEntPropVector(entity, Prop_Data, "m_angRotation", _fAngles);
		AddVectors(_fAngles, _fValue, _fAngles);
		for(new i = 0; i <= 2; i++)
		{			
			while(_fAngles[i] < 0.0)
				_fAngles[i] += 360.0;

			while(_fAngles[i] > 360.0)
				_fAngles[i] -= 360.0;
		}
		TeleportEntity(entity, NULL_VECTOR, _fAngles, NULL_VECTOR);
	}
	else
		TeleportEntity(entity, NULL_VECTOR, _fAngles, NULL_VECTOR);
}

stock Entity_SetXProp(entity, Float:x_cord)
{
	new Float:_fAngles[3];
	
	GetEntPropVector(entity, Prop_Data, "m_angRotation", _fAngles);
	
	_fAngles[0] = x_cord;
	
	TeleportEntity(entity, NULL_VECTOR, _fAngles, NULL_VECTOR);
}

stock Entity_SetYProp(entity, Float:y_cord)
{
	new Float:_fAngles[3];
	
	GetEntPropVector(entity, Prop_Data, "m_angRotation", _fAngles);
	
	_fAngles[1] = y_cord;
	
	TeleportEntity(entity, NULL_VECTOR, _fAngles, NULL_VECTOR);
}

stock Entity_SetZProp(entity, Float:z_cord)
{
	new Float:_fAngles[3];
	
	GetEntPropVector(entity, Prop_Data, "m_angRotation", _fAngles);
	
	_fAngles[2] = z_cord;
	
	TeleportEntity(entity, NULL_VECTOR, _fAngles, NULL_VECTOR);
}

stock Entity_PositionProp(entity, Float:x_cord,Float:y_cord,Float:z_cord)
{
	decl Float:_fOrigin[3];
	new Float:_fValue[3];

	_fValue[0] = x_cord;
	_fValue[1] = y_cord;
	_fValue[2] = z_cord;

	GetEntPropVector(entity, Prop_Data, "m_vecOrigin", _fOrigin);

	AddVectors(_fOrigin, _fValue, _fOrigin);
	TeleportEntity(entity, _fOrigin, NULL_VECTOR, NULL_VECTOR);
}