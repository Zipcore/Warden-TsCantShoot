#if defined _rp_teams_included
 #endinput
#endif
#define _rp_teams_included
	
native RP_SetCop(client, bool:set);
native RP_IsCop(client);
native RP_ChangeCopMode(client);

public __pl_rp_teams_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_SetCop");
	MarkNativeAsOptional("RP_IsCop");
	MarkNativeAsOptional("RP_ChangeCopMode");
}