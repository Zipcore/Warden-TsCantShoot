#if defined _rp_doors_included
 #endinput
#endif
#define _rp_doors_included

enum RP_Doors
{
	Door_ID,
	Door_AlarmID,
	Door_LockID,
	Door_ScannerID,
	Door_SensorID,
	Door_TrapID,
	Door_LocksAmount,
	String:Door_Name[128],
	Door_Entity,
	Float:Door_Pos[3],
	Door_Type,
	Building_ID,
	bool:Door_IsLockedDefault,
	bool:Door_IsLocked
}

enum Door_SetUpHelper
{
	Entity,
	Float:Door_Pos[3]
}

enum
{
	Door_Default,
	Door_Bank_Entrance,
	Door_Bank_WorkerEntrance,
	Door_Bank_Office,
	Door_Bank_ATMDoor,
	Door_Appartment,
	Door_TrainingcenterOffice,
	Door_TrainingcenterWorkerEntrance,
	Door_Trainingcenter,
}

native RP_IsValidDoorEnt(entity);

native RP_GetDoorIDbyEntity(entity);
native RP_IsDoorEntityRegistered(entity);

native RP_GetDoorCount();
native RP_GetDoorIDByArrayIndex(index);
	
native RP_GetDoorAlarmID(doorID);
native RP_GetDoorName(doorID,String:buffer[],length_64);
native RP_GetDoorLockID(doorID);
native RP_GetDoorScannerID(doorID);
native RP_GetDoorSensorID(doorID);
native RP_GetDoorTrapID(doorID);
native RP_GetDoorType(doorID);
native RP_GetDoorBuildingID(doorID);

native RP_IsDoorLockedDefault(doorID);
native RP_IsDoorLocked(doorID);
	
native RP_SetDoorAlarmID(doorID,alarmID);
native RP_SetDoorName(doorID,String:buffer[]);
native RP_SetDoorLockID(doorID,lockID);
native RP_SetDoorScannerID(doorID,scannerID);
native RP_SetDoorSensorID(doorID,sensorID);
native RP_SetDoorTrapID(doorID,trapID);
native RP_SetDoorType(doorID,doortype);
native RP_SetDoorBuildingID(doorID,buildingID);
native RP_SetDoorLockedDefault(doorID,lockedDefault);

native RP_OpenDoor(doorID);
native RP_CloseDoor(doorID);
native RP_LockDoor(doorID);
native RP_UnlockDoor(doorID);

native RP_OpenDoorByEntity(entity);
native RP_CloseDoorByEntity(entity);
native RP_LockDoorByEntity(entity);
native RP_UnlockDoorByEntity(entity);
	
native RP_ResetDoor(doorID);

public __pl_rp_doors_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_IsDoorEntityRegistered");
	
	MarkNativeAsOptional("RP_GetDoorIDbyEntity");
	MarkNativeAsOptional("RP_IsRPDoor");
	
	MarkNativeAsOptional("RP_GetDoorCount");
	MarkNativeAsOptional("RP_GetDoorIDByArrayIndex");
	
	MarkNativeAsOptional("RP_GetDoorAlarmID");
	MarkNativeAsOptional("RP_GetDoorName");
	MarkNativeAsOptional("RP_GetDoorLockID");
	MarkNativeAsOptional("RP_GetDoorScannerID");
	MarkNativeAsOptional("RP_GetDoorSensorID");
	MarkNativeAsOptional("RP_GetDoorTrapID");
	MarkNativeAsOptional("RP_GetDoorType");
	MarkNativeAsOptional("RP_GetDoorBuildingID");
	
	MarkNativeAsOptional("RP_IsDoorLockedDefault");
	MarkNativeAsOptional("RP_IsDoorLocked");
	
	MarkNativeAsOptional("RP_SetDoorAlarmID");
	MarkNativeAsOptional("RP_SetDoorName");
	MarkNativeAsOptional("RP_SetDoorLockID");
	MarkNativeAsOptional("RP_SetDoorScannerID");
	MarkNativeAsOptional("RP_SetDoorSensorID");
	MarkNativeAsOptional("RP_SetDoorTrapID");
	MarkNativeAsOptional("RP_SetDoorType");
	MarkNativeAsOptional("RP_SetDoorBuildingID");
	MarkNativeAsOptional("RP_SetDoorLockedDefault");
	
	MarkNativeAsOptional("RP_OpenDoor");
	MarkNativeAsOptional("RP_CloseDoor");
	MarkNativeAsOptional("RP_LockDoor");
	MarkNativeAsOptional("RP_UnlockDoor");
	
	MarkNativeAsOptional("RP_OpenDoorByEntity");
	MarkNativeAsOptional("RP_CloseDoorByEntity");
	MarkNativeAsOptional("RP_LockDoorByEntity");
	MarkNativeAsOptional("RP_UnlockDoorByEntity");
	
	MarkNativeAsOptional("RP_ResetDoor");
}