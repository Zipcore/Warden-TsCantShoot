#if defined _rp_clash_included
 #endinput
#endif
#define _rp_clash_included

forward RP_OnPlayerKilled(victim, attackerm);
forward RP_OnPlayerHurt(victim, attacker);

forward OnPlayerCombatStart(client);
forward OnPlayerCombatEnd(client);

native RP_ClientCuff(client);
native RP_ClientUncuff(client);
native bool:RP_IsClientCuffed(client);
native RP_ClientToggleCuff(client);
native RP_ClientUnsetJail(client);
native RP_ClientSetJail(client,time,pd,type);
native bool:RP_IsClientInJail(client);
native RP_ClientGetTimeInJail(client);
native RP_ClientSetJailTime(client,time);
native RP_ClientDecJailTime(client);

native RP_IsPlayerInCombat(client);

public __pl_rp_clash_SetNTVOptional() 
{
	MarkNativeAsOptional("RP_IsPlayerInCombat");
	MarkNativeAsOptional("RP_ClientCuff");
	MarkNativeAsOptional("RP_ClientUncuff");
	MarkNativeAsOptional("RP_IsClientCuffed");
	MarkNativeAsOptional("RP_ClientToggleCuff");
	MarkNativeAsOptional("RP_ClientUnsetJail");
	MarkNativeAsOptional("RP_ClientSetJail");
	MarkNativeAsOptional("RP_IsClientInJail");
	MarkNativeAsOptional("RP_ClientGetTimeInJail");
	MarkNativeAsOptional("RP_ClientDecJailTime");
	MarkNativeAsOptional("RP_ClientSetJailTime");
}